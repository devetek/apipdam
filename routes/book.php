<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//header('Access-Control-Allow-Origin: *');
//header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
//header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization,X-Auth-Token");
//date_default_timezone_set('Asia/Jakarta');



Route::middleware('api')->group( function () {
    Route::resource('admin/books', 'admin\BookAPIController');
    Route::resource('admin/bookSections', 'admin\BookSectionAPIController');
    Route::resource('admin/bookContents', 'admin\BookContentAPIController');
    Route::resource('admin/book_covers', 'admin\BookCoverAPIController');
});

Route::middleware('api')->group( function () {
    Route::resource('member/books', 'member\BookAPIController');
    Route::resource('member/bookSections', 'member\BookSectionAPIController');
    Route::resource('member/bookContents', 'member\BookContentAPIController');
});

Route::middleware('api')->group( function () {
    Route::resource('memberGuest/books', 'member_Guest\BookAPIController');
});

//php artisan infyom:api BookContent --fromTable --tableName=book_content --prefix=book/member --primary=id_book_content
