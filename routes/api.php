<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'Api\User\AuthCont@authenticate');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//php artisan infyom:api Slider --fromTable --tableName=gallery --prefix=Slider/member --primary=id_gallery




Route::get('publisher/admin/publisher_covers', 'Publisher/admin\PublisherCoverAPIController@index');
Route::post('publisher/admin/publisher_covers', 'Publisher/admin\PublisherCoverAPIController@store');
Route::get('publisher/admin/publisher_covers/{publisher_covers}', 'Publisher/admin\PublisherCoverAPIController@show');
Route::put('publisher/admin/publisher_covers/{publisher_covers}', 'Publisher/admin\PublisherCoverAPIController@update');
Route::patch('publisher/admin/publisher_covers/{publisher_covers}', 'Publisher/admin\PublisherCoverAPIController@update');
Route::delete('publisher/admin/publisher_covers/{publisher_covers}', 'Publisher/admin\PublisherCoverAPIController@destroy');
