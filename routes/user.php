<?php


Auth::routes();

Route::get('dam_user', 'AuthCont@getRole');
Route::post('logout', 'AuthCont@logout');
Route::post('refresh', 'AuthCont@refresh');
Route::get('cek', 'AuthCont@cek');
Route::get('user/me', 'AuthCont@getAuthenticatedUser');

//php artisan infyom:api Publisher --fromTable --tableName=publisher --prefix=publisher/admin --primary=id_publisher

