<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//header('Access-Control-Allow-Origin: *');
//header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
//header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization,X-Auth-Token");
//date_default_timezone_set('Asia/Jakarta');



Route::middleware('api')->group( function () {
    Route::resource('memberGuest/packets', 'member_Guest\PacketAPIController');
    Route::resource('memberGuest/packet_goals', 'member_Guest\Packet_goalAPIController');
    Route::resource('memberGuest/packet_requirements', 'member_Guest\Packet_requirementsAPIController');
});

//php artisan infyom:api Book --fromTable --tableName=book --prefix=book/admin --primary=id_book
