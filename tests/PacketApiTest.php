<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PacketApiTest extends TestCase
{
    use MakePacketTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreatePacket()
    {
        $packet = $this->fakePacketData();
        $this->json('POST', '/api/v1/packets', $packet);

        $this->assertApiResponse($packet);
    }

    /**
     * @test
     */
    public function testReadPacket()
    {
        $packet = $this->makePacket();
        $this->json('GET', '/api/v1/packets/'.$packet->id_gallery);

        $this->assertApiResponse($packet->toArray());
    }

    /**
     * @test
     */
    public function testUpdatePacket()
    {
        $packet = $this->makePacket();
        $editedPacket = $this->fakePacketData();

        $this->json('PUT', '/api/v1/packets/'.$packet->id_gallery, $editedPacket);

        $this->assertApiResponse($editedPacket);
    }

    /**
     * @test
     */
    public function testDeletePacket()
    {
        $packet = $this->makePacket();
        $this->json('DELETE', '/api/v1/packets/'.$packet->id_gallery);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/packets/'.$packet->id_gallery);

        $this->assertResponseStatus(404);
    }
}
