<?php

use Faker\Factory as Faker;
use App\Models\Packet;
use App\Repositories\Packet\member_Guest\PacketRepository;

trait MakePacketTrait
{
    /**
     * Create fake instance of Packet and save it in database
     *
     * @param array $packetFields
     * @return Packet
     */
    public function makePacket($packetFields = [])
    {
        /** @var PacketRepository $packetRepo */
        $packetRepo = App::make(PacketRepository::class);
        $theme = $this->fakePacketData($packetFields);
        return $packetRepo->create($theme);
    }

    /**
     * Get fake instance of Packet
     *
     * @param array $packetFields
     * @return Packet
     */
    public function fakePacket($packetFields = [])
    {
        return new Packet($this->fakePacketData($packetFields));
    }

    /**
     * Get fake data of Packet
     *
     * @param array $postFields
     * @return array
     */
    public function fakePacketData($packetFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name_packet' => $fake->word,
            'excerpt_packet' => $fake->text,
            'description_packet' => $fake->word,
            'id_publisher_team' => $fake->randomDigitNotNull,
            'cover_packet' => $fake->word,
            'thum_packet' => $fake->word,
            'date_publish' => $fake->word,
            'id_company' => $fake->randomDigitNotNull,
            'status_packet' => $fake->word,
            'is_public' => $fake->word,
            'price' => $fake->randomDigitNotNull,
            'validasi' => $fake->randomDigitNotNull,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s')
        ], $packetFields);
    }
}
