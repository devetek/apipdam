<?php

use Faker\Factory as Faker;
use App\Models\Publisher;
use App\Repositories\Publisher\admin\PublisherRepository;

trait MakePublisherTrait
{
    /**
     * Create fake instance of Publisher and save it in database
     *
     * @param array $publisherFields
     * @return Publisher
     */
    public function makePublisher($publisherFields = [])
    {
        /** @var PublisherRepository $publisherRepo */
        $publisherRepo = App::make(PublisherRepository::class);
        $theme = $this->fakePublisherData($publisherFields);
        return $publisherRepo->create($theme);
    }

    /**
     * Get fake instance of Publisher
     *
     * @param array $publisherFields
     * @return Publisher
     */
    public function fakePublisher($publisherFields = [])
    {
        return new Publisher($this->fakePublisherData($publisherFields));
    }

    /**
     * Get fake data of Publisher
     *
     * @param array $postFields
     * @return array
     */
    public function fakePublisherData($publisherFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'first_name' => $fake->word,
            'last_name' => $fake->word,
            'public_name' => $fake->word,
            'title_publisher' => $fake->word,
            'foto_publisher' => $fake->word,
            'thum_publisher' => $fake->word,
            'phone_number' => $fake->word,
            'mail_publisher' => $fake->word,
            'id_company' => $fake->randomDigitNotNull,
            'join_date' => $fake->word,
            'status_publisher' => $fake->word,
            'validasi' => $fake->randomDigitNotNull,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s')
        ], $publisherFields);
    }
}
