<?php

use Faker\Factory as Faker;
use App\Models\Packet_goal;
use App\Repositories\Packet\member_Guest\Packet_goalRepository;

trait MakePacket_goalTrait
{
    /**
     * Create fake instance of Packet_goal and save it in database
     *
     * @param array $packetGoalFields
     * @return Packet_goal
     */
    public function makePacket_goal($packetGoalFields = [])
    {
        /** @var Packet_goalRepository $packetGoalRepo */
        $packetGoalRepo = App::make(Packet_goalRepository::class);
        $theme = $this->fakePacket_goalData($packetGoalFields);
        return $packetGoalRepo->create($theme);
    }

    /**
     * Get fake instance of Packet_goal
     *
     * @param array $packetGoalFields
     * @return Packet_goal
     */
    public function fakePacket_goal($packetGoalFields = [])
    {
        return new Packet_goal($this->fakePacket_goalData($packetGoalFields));
    }

    /**
     * Get fake data of Packet_goal
     *
     * @param array $postFields
     * @return array
     */
    public function fakePacket_goalData($packetGoalFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'id_packet' => $fake->randomDigitNotNull,
            'order_goal' => $fake->randomDigitNotNull,
            'goal' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s')
        ], $packetGoalFields);
    }
}
