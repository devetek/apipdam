<?php

use Faker\Factory as Faker;
use App\Models\Santri;
use App\Repositories\Admin\SantriRepository;

trait MakeSantriTrait
{
    /**
     * Create fake instance of Santri and save it in database
     *
     * @param array $santriFields
     * @return Santri
     */
    public function makeSantri($santriFields = [])
    {
        /** @var SantriRepository $santriRepo */
        $santriRepo = App::make(SantriRepository::class);
        $theme = $this->fakeSantriData($santriFields);
        return $santriRepo->create($theme);
    }

    /**
     * Get fake instance of Santri
     *
     * @param array $santriFields
     * @return Santri
     */
    public function fakeSantri($santriFields = [])
    {
        return new Santri($this->fakeSantriData($santriFields));
    }

    /**
     * Get fake data of Santri
     *
     * @param array $postFields
     * @return array
     */
    public function fakeSantriData($santriFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'id_user' => $fake->randomDigitNotNull,
            'nama_lengkap' => $fake->word,
            'alamat' => $fake->word,
            'region' => $fake->word,
            'no_hp' => $fake->word,
            'kode_pos' => $fake->randomDigitNotNull,
            'pendidikan_terakhir' => $fake->word,
            'is_active' => $fake->randomDigitNotNull,
            'validasi' => $fake->randomDigitNotNull,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s')
        ], $santriFields);
    }
}
