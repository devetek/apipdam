<?php

use Faker\Factory as Faker;
use App\Models\Company;
use App\Repositories\Company\member_Guest\CompanyRepository;

trait MakeCompanyTrait
{
    /**
     * Create fake instance of Company and save it in database
     *
     * @param array $companyFields
     * @return Company
     */
    public function makeCompany($companyFields = [])
    {
        /** @var CompanyRepository $companyRepo */
        $companyRepo = App::make(CompanyRepository::class);
        $theme = $this->fakeCompanyData($companyFields);
        return $companyRepo->create($theme);
    }

    /**
     * Get fake instance of Company
     *
     * @param array $companyFields
     * @return Company
     */
    public function fakeCompany($companyFields = [])
    {
        return new Company($this->fakeCompanyData($companyFields));
    }

    /**
     * Get fake data of Company
     *
     * @param array $postFields
     * @return array
     */
    public function fakeCompanyData($companyFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name_company' => $fake->word,
            'logo_company' => $fake->word,
            'thum_company' => $fake->word,
            'address_company' => $fake->text,
            'about_company' => $fake->text,
            'link_company' => $fake->word,
            'id_head_company' => $fake->word,
            'phone_company' => $fake->word,
            'status_company' => $fake->word,
            'id_user' => $fake->randomDigitNotNull,
            'validasi' => $fake->randomDigitNotNull,
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'created_at' => $fake->date('Y-m-d H:i:s')
        ], $companyFields);
    }
}
