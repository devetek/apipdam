<?php

use Faker\Factory as Faker;
use App\Models\Packet_master_category;
use App\Repositories\Packet\admin\Packet_master_categoryRepository;

trait MakePacket_master_categoryTrait
{
    /**
     * Create fake instance of Packet_master_category and save it in database
     *
     * @param array $packetMasterCategoryFields
     * @return Packet_master_category
     */
    public function makePacket_master_category($packetMasterCategoryFields = [])
    {
        /** @var Packet_master_categoryRepository $packetMasterCategoryRepo */
        $packetMasterCategoryRepo = App::make(Packet_master_categoryRepository::class);
        $theme = $this->fakePacket_master_categoryData($packetMasterCategoryFields);
        return $packetMasterCategoryRepo->create($theme);
    }

    /**
     * Get fake instance of Packet_master_category
     *
     * @param array $packetMasterCategoryFields
     * @return Packet_master_category
     */
    public function fakePacket_master_category($packetMasterCategoryFields = [])
    {
        return new Packet_master_category($this->fakePacket_master_categoryData($packetMasterCategoryFields));
    }

    /**
     * Get fake data of Packet_master_category
     *
     * @param array $postFields
     * @return array
     */
    public function fakePacket_master_categoryData($packetMasterCategoryFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name_category' => $fake->word,
            'description_category' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s')
        ], $packetMasterCategoryFields);
    }
}
