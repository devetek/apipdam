<?php

use App\Models\BookContent;
use App\Repositories\Book\member\BookContentRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class BookContentRepositoryTest extends TestCase
{
    use MakeBookContentTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var BookContentRepository
     */
    protected $bookContentRepo;

    public function setUp()
    {
        parent::setUp();
        $this->bookContentRepo = App::make(BookContentRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateBookContent()
    {
        $bookContent = $this->fakeBookContentData();
        $createdBookContent = $this->bookContentRepo->create($bookContent);
        $createdBookContent = $createdBookContent->toArray();
        $this->assertArrayHasKey('id', $createdBookContent);
        $this->assertNotNull($createdBookContent['id'], 'Created BookContent must have id specified');
        $this->assertNotNull(BookContent::find($createdBookContent['id']), 'BookContent with given id must be in DB');
        $this->assertModelData($bookContent, $createdBookContent);
    }

    /**
     * @test read
     */
    public function testReadBookContent()
    {
        $bookContent = $this->makeBookContent();
        $dbBookContent = $this->bookContentRepo->find($bookContent->id_book_content);
        $dbBookContent = $dbBookContent->toArray();
        $this->assertModelData($bookContent->toArray(), $dbBookContent);
    }

    /**
     * @test update
     */
    public function testUpdateBookContent()
    {
        $bookContent = $this->makeBookContent();
        $fakeBookContent = $this->fakeBookContentData();
        $updatedBookContent = $this->bookContentRepo->update($fakeBookContent, $bookContent->id_book_content);
        $this->assertModelData($fakeBookContent, $updatedBookContent->toArray());
        $dbBookContent = $this->bookContentRepo->find($bookContent->id_book_content);
        $this->assertModelData($fakeBookContent, $dbBookContent->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteBookContent()
    {
        $bookContent = $this->makeBookContent();
        $resp = $this->bookContentRepo->delete($bookContent->id_book_content);
        $this->assertTrue($resp);
        $this->assertNull(BookContent::find($bookContent->id_book_content), 'BookContent should not exist in DB');
    }
}
