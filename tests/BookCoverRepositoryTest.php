<?php

use App\Models\BookCover;
use App\Repositories\Book\admin\BookCoverRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class BookCoverRepositoryTest extends TestCase
{
    use MakeBookCoverTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var BookCoverRepository
     */
    protected $bookCoverRepo;

    public function setUp()
    {
        parent::setUp();
        $this->bookCoverRepo = App::make(BookCoverRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateBookCover()
    {
        $bookCover = $this->fakeBookCoverData();
        $createdBookCover = $this->bookCoverRepo->create($bookCover);
        $createdBookCover = $createdBookCover->toArray();
        $this->assertArrayHasKey('id', $createdBookCover);
        $this->assertNotNull($createdBookCover['id'], 'Created BookCover must have id specified');
        $this->assertNotNull(BookCover::find($createdBookCover['id']), 'BookCover with given id must be in DB');
        $this->assertModelData($bookCover, $createdBookCover);
    }

    /**
     * @test read
     */
    public function testReadBookCover()
    {
        $bookCover = $this->makeBookCover();
        $dbBookCover = $this->bookCoverRepo->find($bookCover->id_book);
        $dbBookCover = $dbBookCover->toArray();
        $this->assertModelData($bookCover->toArray(), $dbBookCover);
    }

    /**
     * @test update
     */
    public function testUpdateBookCover()
    {
        $bookCover = $this->makeBookCover();
        $fakeBookCover = $this->fakeBookCoverData();
        $updatedBookCover = $this->bookCoverRepo->update($fakeBookCover, $bookCover->id_book);
        $this->assertModelData($fakeBookCover, $updatedBookCover->toArray());
        $dbBookCover = $this->bookCoverRepo->find($bookCover->id_book);
        $this->assertModelData($fakeBookCover, $dbBookCover->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteBookCover()
    {
        $bookCover = $this->makeBookCover();
        $resp = $this->bookCoverRepo->delete($bookCover->id_book);
        $this->assertTrue($resp);
        $this->assertNull(BookCover::find($bookCover->id_book), 'BookCover should not exist in DB');
    }
}
