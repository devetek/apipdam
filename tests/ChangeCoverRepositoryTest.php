<?php

use App\Models\ChangeCover;
use App\Repositories\Book\admin\ChangeCoverRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ChangeCoverRepositoryTest extends TestCase
{
    use MakeChangeCoverTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var ChangeCoverRepository
     */
    protected $changeCoverRepo;

    public function setUp()
    {
        parent::setUp();
        $this->changeCoverRepo = App::make(ChangeCoverRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateChangeCover()
    {
        $changeCover = $this->fakeChangeCoverData();
        $createdChangeCover = $this->changeCoverRepo->create($changeCover);
        $createdChangeCover = $createdChangeCover->toArray();
        $this->assertArrayHasKey('id', $createdChangeCover);
        $this->assertNotNull($createdChangeCover['id'], 'Created ChangeCover must have id specified');
        $this->assertNotNull(ChangeCover::find($createdChangeCover['id']), 'ChangeCover with given id must be in DB');
        $this->assertModelData($changeCover, $createdChangeCover);
    }

    /**
     * @test read
     */
    public function testReadChangeCover()
    {
        $changeCover = $this->makeChangeCover();
        $dbChangeCover = $this->changeCoverRepo->find($changeCover->id);
        $dbChangeCover = $dbChangeCover->toArray();
        $this->assertModelData($changeCover->toArray(), $dbChangeCover);
    }

    /**
     * @test update
     */
    public function testUpdateChangeCover()
    {
        $changeCover = $this->makeChangeCover();
        $fakeChangeCover = $this->fakeChangeCoverData();
        $updatedChangeCover = $this->changeCoverRepo->update($fakeChangeCover, $changeCover->id);
        $this->assertModelData($fakeChangeCover, $updatedChangeCover->toArray());
        $dbChangeCover = $this->changeCoverRepo->find($changeCover->id);
        $this->assertModelData($fakeChangeCover, $dbChangeCover->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteChangeCover()
    {
        $changeCover = $this->makeChangeCover();
        $resp = $this->changeCoverRepo->delete($changeCover->id);
        $this->assertTrue($resp);
        $this->assertNull(ChangeCover::find($changeCover->id), 'ChangeCover should not exist in DB');
    }
}
