<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class BookAdminApiTest extends TestCase
{
    use MakeBookAdminTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateBookAdmin()
    {
        $bookAdmin = $this->fakeBookAdminData();
        $this->json('POST', '/api/v1/bookAdmins', $bookAdmin);

        $this->assertApiResponse($bookAdmin);
    }

    /**
     * @test
     */
    public function testReadBookAdmin()
    {
        $bookAdmin = $this->makeBookAdmin();
        $this->json('GET', '/api/v1/bookAdmins/'.$bookAdmin->id_book);

        $this->assertApiResponse($bookAdmin->toArray());
    }

    /**
     * @test
     */
    public function testUpdateBookAdmin()
    {
        $bookAdmin = $this->makeBookAdmin();
        $editedBookAdmin = $this->fakeBookAdminData();

        $this->json('PUT', '/api/v1/bookAdmins/'.$bookAdmin->id_book, $editedBookAdmin);

        $this->assertApiResponse($editedBookAdmin);
    }

    /**
     * @test
     */
    public function testDeleteBookAdmin()
    {
        $bookAdmin = $this->makeBookAdmin();
        $this->json('DELETE', '/api/v1/bookAdmins/'.$bookAdmin->id_book);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/bookAdmins/'.$bookAdmin->id_book);

        $this->assertResponseStatus(404);
    }
}
