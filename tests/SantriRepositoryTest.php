<?php

use App\Models\Santri;
use App\Repositories\Admin\SantriRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SantriRepositoryTest extends TestCase
{
    use MakeSantriTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var SantriRepository
     */
    protected $santriRepo;

    public function setUp()
    {
        parent::setUp();
        $this->santriRepo = App::make(SantriRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateSantri()
    {
        $santri = $this->fakeSantriData();
        $createdSantri = $this->santriRepo->create($santri);
        $createdSantri = $createdSantri->toArray();
        $this->assertArrayHasKey('id', $createdSantri);
        $this->assertNotNull($createdSantri['id'], 'Created Santri must have id specified');
        $this->assertNotNull(Santri::find($createdSantri['id']), 'Santri with given id must be in DB');
        $this->assertModelData($santri, $createdSantri);
    }

    /**
     * @test read
     */
    public function testReadSantri()
    {
        $santri = $this->makeSantri();
        $dbSantri = $this->santriRepo->find($santri->id_santri);
        $dbSantri = $dbSantri->toArray();
        $this->assertModelData($santri->toArray(), $dbSantri);
    }

    /**
     * @test update
     */
    public function testUpdateSantri()
    {
        $santri = $this->makeSantri();
        $fakeSantri = $this->fakeSantriData();
        $updatedSantri = $this->santriRepo->update($fakeSantri, $santri->id_santri);
        $this->assertModelData($fakeSantri, $updatedSantri->toArray());
        $dbSantri = $this->santriRepo->find($santri->id_santri);
        $this->assertModelData($fakeSantri, $dbSantri->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteSantri()
    {
        $santri = $this->makeSantri();
        $resp = $this->santriRepo->delete($santri->id_santri);
        $this->assertTrue($resp);
        $this->assertNull(Santri::find($santri->id_santri), 'Santri should not exist in DB');
    }
}
