<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Packet_requirementsApiTest extends TestCase
{
    use MakePacket_requirementsTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreatePacket_requirements()
    {
        $packetRequirements = $this->fakePacket_requirementsData();
        $this->json('POST', '/api/v1/packetRequirements', $packetRequirements);

        $this->assertApiResponse($packetRequirements);
    }

    /**
     * @test
     */
    public function testReadPacket_requirements()
    {
        $packetRequirements = $this->makePacket_requirements();
        $this->json('GET', '/api/v1/packetRequirements/'.$packetRequirements->id_packet_requirements);

        $this->assertApiResponse($packetRequirements->toArray());
    }

    /**
     * @test
     */
    public function testUpdatePacket_requirements()
    {
        $packetRequirements = $this->makePacket_requirements();
        $editedPacket_requirements = $this->fakePacket_requirementsData();

        $this->json('PUT', '/api/v1/packetRequirements/'.$packetRequirements->id_packet_requirements, $editedPacket_requirements);

        $this->assertApiResponse($editedPacket_requirements);
    }

    /**
     * @test
     */
    public function testDeletePacket_requirements()
    {
        $packetRequirements = $this->makePacket_requirements();
        $this->json('DELETE', '/api/v1/packetRequirements/'.$packetRequirements->id_packet_requirements);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/packetRequirements/'.$packetRequirements->id_packet_requirements);

        $this->assertResponseStatus(404);
    }
}
