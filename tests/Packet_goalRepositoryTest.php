<?php

use App\Models\Packet_goal;
use App\Repositories\Packet\member_Guest\Packet_goalRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Packet_goalRepositoryTest extends TestCase
{
    use MakePacket_goalTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var Packet_goalRepository
     */
    protected $packetGoalRepo;

    public function setUp()
    {
        parent::setUp();
        $this->packetGoalRepo = App::make(Packet_goalRepository::class);
    }

    /**
     * @test create
     */
    public function testCreatePacket_goal()
    {
        $packetGoal = $this->fakePacket_goalData();
        $createdPacket_goal = $this->packetGoalRepo->create($packetGoal);
        $createdPacket_goal = $createdPacket_goal->toArray();
        $this->assertArrayHasKey('id', $createdPacket_goal);
        $this->assertNotNull($createdPacket_goal['id'], 'Created Packet_goal must have id specified');
        $this->assertNotNull(Packet_goal::find($createdPacket_goal['id']), 'Packet_goal with given id must be in DB');
        $this->assertModelData($packetGoal, $createdPacket_goal);
    }

    /**
     * @test read
     */
    public function testReadPacket_goal()
    {
        $packetGoal = $this->makePacket_goal();
        $dbPacket_goal = $this->packetGoalRepo->find($packetGoal->id_packet_goal);
        $dbPacket_goal = $dbPacket_goal->toArray();
        $this->assertModelData($packetGoal->toArray(), $dbPacket_goal);
    }

    /**
     * @test update
     */
    public function testUpdatePacket_goal()
    {
        $packetGoal = $this->makePacket_goal();
        $fakePacket_goal = $this->fakePacket_goalData();
        $updatedPacket_goal = $this->packetGoalRepo->update($fakePacket_goal, $packetGoal->id_packet_goal);
        $this->assertModelData($fakePacket_goal, $updatedPacket_goal->toArray());
        $dbPacket_goal = $this->packetGoalRepo->find($packetGoal->id_packet_goal);
        $this->assertModelData($fakePacket_goal, $dbPacket_goal->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletePacket_goal()
    {
        $packetGoal = $this->makePacket_goal();
        $resp = $this->packetGoalRepo->delete($packetGoal->id_packet_goal);
        $this->assertTrue($resp);
        $this->assertNull(Packet_goal::find($packetGoal->id_packet_goal), 'Packet_goal should not exist in DB');
    }
}
