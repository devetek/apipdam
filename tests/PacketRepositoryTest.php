<?php

use App\Models\Packet;
use App\Repositories\Packet\member_Guest\PacketRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PacketRepositoryTest extends TestCase
{
    use MakePacketTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var PacketRepository
     */
    protected $packetRepo;

    public function setUp()
    {
        parent::setUp();
        $this->packetRepo = App::make(PacketRepository::class);
    }

    /**
     * @test create
     */
    public function testCreatePacket()
    {
        $packet = $this->fakePacketData();
        $createdPacket = $this->packetRepo->create($packet);
        $createdPacket = $createdPacket->toArray();
        $this->assertArrayHasKey('id', $createdPacket);
        $this->assertNotNull($createdPacket['id'], 'Created Packet must have id specified');
        $this->assertNotNull(Packet::find($createdPacket['id']), 'Packet with given id must be in DB');
        $this->assertModelData($packet, $createdPacket);
    }

    /**
     * @test read
     */
    public function testReadPacket()
    {
        $packet = $this->makePacket();
        $dbPacket = $this->packetRepo->find($packet->id_gallery);
        $dbPacket = $dbPacket->toArray();
        $this->assertModelData($packet->toArray(), $dbPacket);
    }

    /**
     * @test update
     */
    public function testUpdatePacket()
    {
        $packet = $this->makePacket();
        $fakePacket = $this->fakePacketData();
        $updatedPacket = $this->packetRepo->update($fakePacket, $packet->id_gallery);
        $this->assertModelData($fakePacket, $updatedPacket->toArray());
        $dbPacket = $this->packetRepo->find($packet->id_gallery);
        $this->assertModelData($fakePacket, $dbPacket->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletePacket()
    {
        $packet = $this->makePacket();
        $resp = $this->packetRepo->delete($packet->id_gallery);
        $this->assertTrue($resp);
        $this->assertNull(Packet::find($packet->id_gallery), 'Packet should not exist in DB');
    }
}
