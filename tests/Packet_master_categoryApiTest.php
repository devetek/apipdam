<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Packet_master_categoryApiTest extends TestCase
{
    use MakePacket_master_categoryTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreatePacket_master_category()
    {
        $packetMasterCategory = $this->fakePacket_master_categoryData();
        $this->json('POST', '/api/v1/packetMasterCategories', $packetMasterCategory);

        $this->assertApiResponse($packetMasterCategory);
    }

    /**
     * @test
     */
    public function testReadPacket_master_category()
    {
        $packetMasterCategory = $this->makePacket_master_category();
        $this->json('GET', '/api/v1/packetMasterCategories/'.$packetMasterCategory->id_packet_master_category);

        $this->assertApiResponse($packetMasterCategory->toArray());
    }

    /**
     * @test
     */
    public function testUpdatePacket_master_category()
    {
        $packetMasterCategory = $this->makePacket_master_category();
        $editedPacket_master_category = $this->fakePacket_master_categoryData();

        $this->json('PUT', '/api/v1/packetMasterCategories/'.$packetMasterCategory->id_packet_master_category, $editedPacket_master_category);

        $this->assertApiResponse($editedPacket_master_category);
    }

    /**
     * @test
     */
    public function testDeletePacket_master_category()
    {
        $packetMasterCategory = $this->makePacket_master_category();
        $this->json('DELETE', '/api/v1/packetMasterCategories/'.$packetMasterCategory->id_packet_master_category);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/packetMasterCategories/'.$packetMasterCategory->id_packet_master_category);

        $this->assertResponseStatus(404);
    }
}
