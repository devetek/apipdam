<?php

use App\Models\packetAdmin;
use App\Repositories\Packet\packetAdminRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class packetAdminRepositoryTest extends TestCase
{
    use MakepacketAdminTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var packetAdminRepository
     */
    protected $packetAdminRepo;

    public function setUp()
    {
        parent::setUp();
        $this->packetAdminRepo = App::make(packetAdminRepository::class);
    }

    /**
     * @test create
     */
    public function testCreatepacketAdmin()
    {
        $packetAdmin = $this->fakepacketAdminData();
        $createdpacketAdmin = $this->packetAdminRepo->create($packetAdmin);
        $createdpacketAdmin = $createdpacketAdmin->toArray();
        $this->assertArrayHasKey('id', $createdpacketAdmin);
        $this->assertNotNull($createdpacketAdmin['id'], 'Created packetAdmin must have id specified');
        $this->assertNotNull(packetAdmin::find($createdpacketAdmin['id']), 'packetAdmin with given id must be in DB');
        $this->assertModelData($packetAdmin, $createdpacketAdmin);
    }

    /**
     * @test read
     */
    public function testReadpacketAdmin()
    {
        $packetAdmin = $this->makepacketAdmin();
        $dbpacketAdmin = $this->packetAdminRepo->find($packetAdmin->id_packet);
        $dbpacketAdmin = $dbpacketAdmin->toArray();
        $this->assertModelData($packetAdmin->toArray(), $dbpacketAdmin);
    }

    /**
     * @test update
     */
    public function testUpdatepacketAdmin()
    {
        $packetAdmin = $this->makepacketAdmin();
        $fakepacketAdmin = $this->fakepacketAdminData();
        $updatedpacketAdmin = $this->packetAdminRepo->update($fakepacketAdmin, $packetAdmin->id_packet);
        $this->assertModelData($fakepacketAdmin, $updatedpacketAdmin->toArray());
        $dbpacketAdmin = $this->packetAdminRepo->find($packetAdmin->id_packet);
        $this->assertModelData($fakepacketAdmin, $dbpacketAdmin->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletepacketAdmin()
    {
        $packetAdmin = $this->makepacketAdmin();
        $resp = $this->packetAdminRepo->delete($packetAdmin->id_packet);
        $this->assertTrue($resp);
        $this->assertNull(packetAdmin::find($packetAdmin->id_packet), 'packetAdmin should not exist in DB');
    }
}
