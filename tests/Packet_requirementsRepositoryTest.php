<?php

use App\Models\Packet_requirements;
use App\Repositories\Packet\member_Guest\Packet_requirementsRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Packet_requirementsRepositoryTest extends TestCase
{
    use MakePacket_requirementsTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var Packet_requirementsRepository
     */
    protected $packetRequirementsRepo;

    public function setUp()
    {
        parent::setUp();
        $this->packetRequirementsRepo = App::make(Packet_requirementsRepository::class);
    }

    /**
     * @test create
     */
    public function testCreatePacket_requirements()
    {
        $packetRequirements = $this->fakePacket_requirementsData();
        $createdPacket_requirements = $this->packetRequirementsRepo->create($packetRequirements);
        $createdPacket_requirements = $createdPacket_requirements->toArray();
        $this->assertArrayHasKey('id', $createdPacket_requirements);
        $this->assertNotNull($createdPacket_requirements['id'], 'Created Packet_requirements must have id specified');
        $this->assertNotNull(Packet_requirements::find($createdPacket_requirements['id']), 'Packet_requirements with given id must be in DB');
        $this->assertModelData($packetRequirements, $createdPacket_requirements);
    }

    /**
     * @test read
     */
    public function testReadPacket_requirements()
    {
        $packetRequirements = $this->makePacket_requirements();
        $dbPacket_requirements = $this->packetRequirementsRepo->find($packetRequirements->id_packet_requirements);
        $dbPacket_requirements = $dbPacket_requirements->toArray();
        $this->assertModelData($packetRequirements->toArray(), $dbPacket_requirements);
    }

    /**
     * @test update
     */
    public function testUpdatePacket_requirements()
    {
        $packetRequirements = $this->makePacket_requirements();
        $fakePacket_requirements = $this->fakePacket_requirementsData();
        $updatedPacket_requirements = $this->packetRequirementsRepo->update($fakePacket_requirements, $packetRequirements->id_packet_requirements);
        $this->assertModelData($fakePacket_requirements, $updatedPacket_requirements->toArray());
        $dbPacket_requirements = $this->packetRequirementsRepo->find($packetRequirements->id_packet_requirements);
        $this->assertModelData($fakePacket_requirements, $dbPacket_requirements->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletePacket_requirements()
    {
        $packetRequirements = $this->makePacket_requirements();
        $resp = $this->packetRequirementsRepo->delete($packetRequirements->id_packet_requirements);
        $this->assertTrue($resp);
        $this->assertNull(Packet_requirements::find($packetRequirements->id_packet_requirements), 'Packet_requirements should not exist in DB');
    }
}
