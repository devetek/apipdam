<?php

use App\Models\Packet_master_category;
use App\Repositories\Packet\admin\Packet_master_categoryRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Packet_master_categoryRepositoryTest extends TestCase
{
    use MakePacket_master_categoryTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var Packet_master_categoryRepository
     */
    protected $packetMasterCategoryRepo;

    public function setUp()
    {
        parent::setUp();
        $this->packetMasterCategoryRepo = App::make(Packet_master_categoryRepository::class);
    }

    /**
     * @test create
     */
    public function testCreatePacket_master_category()
    {
        $packetMasterCategory = $this->fakePacket_master_categoryData();
        $createdPacket_master_category = $this->packetMasterCategoryRepo->create($packetMasterCategory);
        $createdPacket_master_category = $createdPacket_master_category->toArray();
        $this->assertArrayHasKey('id', $createdPacket_master_category);
        $this->assertNotNull($createdPacket_master_category['id'], 'Created Packet_master_category must have id specified');
        $this->assertNotNull(Packet_master_category::find($createdPacket_master_category['id']), 'Packet_master_category with given id must be in DB');
        $this->assertModelData($packetMasterCategory, $createdPacket_master_category);
    }

    /**
     * @test read
     */
    public function testReadPacket_master_category()
    {
        $packetMasterCategory = $this->makePacket_master_category();
        $dbPacket_master_category = $this->packetMasterCategoryRepo->find($packetMasterCategory->id_packet_master_category);
        $dbPacket_master_category = $dbPacket_master_category->toArray();
        $this->assertModelData($packetMasterCategory->toArray(), $dbPacket_master_category);
    }

    /**
     * @test update
     */
    public function testUpdatePacket_master_category()
    {
        $packetMasterCategory = $this->makePacket_master_category();
        $fakePacket_master_category = $this->fakePacket_master_categoryData();
        $updatedPacket_master_category = $this->packetMasterCategoryRepo->update($fakePacket_master_category, $packetMasterCategory->id_packet_master_category);
        $this->assertModelData($fakePacket_master_category, $updatedPacket_master_category->toArray());
        $dbPacket_master_category = $this->packetMasterCategoryRepo->find($packetMasterCategory->id_packet_master_category);
        $this->assertModelData($fakePacket_master_category, $dbPacket_master_category->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletePacket_master_category()
    {
        $packetMasterCategory = $this->makePacket_master_category();
        $resp = $this->packetMasterCategoryRepo->delete($packetMasterCategory->id_packet_master_category);
        $this->assertTrue($resp);
        $this->assertNull(Packet_master_category::find($packetMasterCategory->id_packet_master_category), 'Packet_master_category should not exist in DB');
    }
}
