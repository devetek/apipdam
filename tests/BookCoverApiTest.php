<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class BookCoverApiTest extends TestCase
{
    use MakeBookCoverTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateBookCover()
    {
        $bookCover = $this->fakeBookCoverData();
        $this->json('POST', '/api/v1/bookCovers', $bookCover);

        $this->assertApiResponse($bookCover);
    }

    /**
     * @test
     */
    public function testReadBookCover()
    {
        $bookCover = $this->makeBookCover();
        $this->json('GET', '/api/v1/bookCovers/'.$bookCover->id_book);

        $this->assertApiResponse($bookCover->toArray());
    }

    /**
     * @test
     */
    public function testUpdateBookCover()
    {
        $bookCover = $this->makeBookCover();
        $editedBookCover = $this->fakeBookCoverData();

        $this->json('PUT', '/api/v1/bookCovers/'.$bookCover->id_book, $editedBookCover);

        $this->assertApiResponse($editedBookCover);
    }

    /**
     * @test
     */
    public function testDeleteBookCover()
    {
        $bookCover = $this->makeBookCover();
        $this->json('DELETE', '/api/v1/bookCovers/'.$bookCover->id_book);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/bookCovers/'.$bookCover->id_book);

        $this->assertResponseStatus(404);
    }
}
