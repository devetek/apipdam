<?php

use App\Models\Slider_img;
use App\Repositories\Slider\member\Slider_imgRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Slider_imgRepositoryTest extends TestCase
{
    use MakeSlider_imgTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var Slider_imgRepository
     */
    protected $sliderImgRepo;

    public function setUp()
    {
        parent::setUp();
        $this->sliderImgRepo = App::make(Slider_imgRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateSlider_img()
    {
        $sliderImg = $this->fakeSlider_imgData();
        $createdSlider_img = $this->sliderImgRepo->create($sliderImg);
        $createdSlider_img = $createdSlider_img->toArray();
        $this->assertArrayHasKey('id', $createdSlider_img);
        $this->assertNotNull($createdSlider_img['id'], 'Created Slider_img must have id specified');
        $this->assertNotNull(Slider_img::find($createdSlider_img['id']), 'Slider_img with given id must be in DB');
        $this->assertModelData($sliderImg, $createdSlider_img);
    }

    /**
     * @test read
     */
    public function testReadSlider_img()
    {
        $sliderImg = $this->makeSlider_img();
        $dbSlider_img = $this->sliderImgRepo->find($sliderImg->id_gallery_img);
        $dbSlider_img = $dbSlider_img->toArray();
        $this->assertModelData($sliderImg->toArray(), $dbSlider_img);
    }

    /**
     * @test update
     */
    public function testUpdateSlider_img()
    {
        $sliderImg = $this->makeSlider_img();
        $fakeSlider_img = $this->fakeSlider_imgData();
        $updatedSlider_img = $this->sliderImgRepo->update($fakeSlider_img, $sliderImg->id_gallery_img);
        $this->assertModelData($fakeSlider_img, $updatedSlider_img->toArray());
        $dbSlider_img = $this->sliderImgRepo->find($sliderImg->id_gallery_img);
        $this->assertModelData($fakeSlider_img, $dbSlider_img->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteSlider_img()
    {
        $sliderImg = $this->makeSlider_img();
        $resp = $this->sliderImgRepo->delete($sliderImg->id_gallery_img);
        $this->assertTrue($resp);
        $this->assertNull(Slider_img::find($sliderImg->id_gallery_img), 'Slider_img should not exist in DB');
    }
}
