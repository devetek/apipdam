<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class packetAdminApiTest extends TestCase
{
    use MakepacketAdminTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreatepacketAdmin()
    {
        $packetAdmin = $this->fakepacketAdminData();
        $this->json('POST', '/api/v1/packetAdmins', $packetAdmin);

        $this->assertApiResponse($packetAdmin);
    }

    /**
     * @test
     */
    public function testReadpacketAdmin()
    {
        $packetAdmin = $this->makepacketAdmin();
        $this->json('GET', '/api/v1/packetAdmins/'.$packetAdmin->id_packet);

        $this->assertApiResponse($packetAdmin->toArray());
    }

    /**
     * @test
     */
    public function testUpdatepacketAdmin()
    {
        $packetAdmin = $this->makepacketAdmin();
        $editedpacketAdmin = $this->fakepacketAdminData();

        $this->json('PUT', '/api/v1/packetAdmins/'.$packetAdmin->id_packet, $editedpacketAdmin);

        $this->assertApiResponse($editedpacketAdmin);
    }

    /**
     * @test
     */
    public function testDeletepacketAdmin()
    {
        $packetAdmin = $this->makepacketAdmin();
        $this->json('DELETE', '/api/v1/packetAdmins/'.$packetAdmin->id_packet);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/packetAdmins/'.$packetAdmin->id_packet);

        $this->assertResponseStatus(404);
    }
}
