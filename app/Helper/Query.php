<?php

namespace App\Helper; 

use Illuminate\Support\Facades\DB;

class Query
{

    public static function filterRequest($model,$request)
    {
         
        $sql=" ";

        $req = $request->input();
        foreach($req as $key=>$x)
        {
            if(in_array(strtolower($key), $model) )
            {
                
                $sql.=" and ".$key." ".self::parameter($x)." ";
            }
            
        }

        return $sql;
 
 
    }

    static function parameter($param)
    {

        if (strpos($param, '%') !== false) {
            return " like '%". (str_replace('%','',$param)) ."%' ";
        }

        return " = '".$param."'";
    }


    public static function getUser()
    {
        $user = (Object)[];
        $user->id = 1;
        $user->usernama = 'arivin29';
        return $user;
    }
    public static function beforeInsert($model)
    {
        $sql = " Select * from ".$model->table." where validasi < 1 and id_user=".self::getUser()->id;
        $cek = DB::select($sql);
        if(count($cek) < 1 )
        {
            $model->id_user = self::getUser()->id;
            $model->validasi = 0;
            $model->save();

            $cek=$cek = DB::select($sql)[0];
        }
        else
        {
            $cek = $cek[0];
        }

        return $cek;

    }

    public static function afterProsess($data)
    {

    }
}

