<?php

namespace App\Http\Requests\API\Packet\admin;

use App\Models\Packet_master_category;
use InfyOm\Generator\Request\APIRequest;

class CreatePacket_master_categoryAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return Packet_master_category::$rules;
    }
}
