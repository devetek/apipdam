<?php

namespace App\Http\Requests\API\Publisher\admin;

use App\Models\PublisherCover;
use InfyOm\Generator\Request\APIRequest;

class CreatePublisherCoverAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return PublisherCover::$rules;
    }
}
