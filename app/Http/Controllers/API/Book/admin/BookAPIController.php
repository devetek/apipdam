<?php

namespace App\Http\Controllers\API\Book\admin;

use App\Models\Book;
use App\Models\Publisher;
use App\Repositories\Book\admin\BookRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class BookController
 * @package book/admin/ App\Http\Controllers\API\Book\admin
 */

class BookAPIController extends AppBaseController
{
    /** @var  BookRepository */
    private $bookRepository;

    public function __construct(BookRepository $bookRepo)
    {
        $this->bookRepository = $bookRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/book/admin/books",
     *      summary="Get a listing of the Books.",
     *      tags={"Book/admin/Book"},
     *      description="Get all Books",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/V_book")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */

    public function index(Request $request)
    {
        $books = $this->bookRepository->all($request);
        return $this->sendResponse($books, 'Books retrieved successfully');
    }

    public function create(Request $reques)
    {
        $data['publisher'] = Publisher::where('validasi',1)->get();
        return $this->sendResponse($data, 'Books retrieved successfully');
    }

    /**
     * @param CreateBookAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/book/admin/books",
     *      summary="Store a newly created Book in storage",
     *      tags={"Book/admin/Book"},
     *      description="Store Book",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Book that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Book")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/V_book"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(Request $request)
    {
            $input = $request->all();

            $books = $this->bookRepository->create($input);

            return $this->sendResponse($books, 'Book saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/book/admin/books/{id}",
     *      summary="Display the specified Book",
     *      tags={"Book/admin/Book"},
     *      description="Get Book",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Book",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Book"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Book $book */
        $book = $this->bookRepository->findWithoutFail($id);

        if (empty($book)) {
            return $this->sendError('Book not found');
        }

        return $this->sendResponse($book->toArray(), 'Book retrieved successfully');
    }


    /**
     * @param int $id
     * @param UpdateBookAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/book/admin/books/{id}",
     *      summary="Update the specified Book in storage",
     *      tags={"Book/admin/Book"},
     *      description="Update Book",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Book",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Book that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Book")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Book"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, Request $request)
    {
        $input = $request->all();

        /** @var Book $book */
        $book = $this->bookRepository->findWithoutFail($id);

        if (empty($book)) {
            return $this->sendError('Book not found');
        }

        $book = $this->bookRepository->update($input, $id);

        return $this->sendResponse($book->toArray(), 'Book updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/book/admin/books/{id}",
     *      summary="Remove the specified Book from storage",
     *      tags={"Book/admin/Book"},
     *      description="Delete Book",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Book",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Book $book */
        $book = $this->bookRepository->findWithoutFail($id);

        if (empty($book)) {
            return $this->sendError('Book not found');
        }

        $book->delete();

        return $this->sendResponse($id, 'Book deleted successfully');
    }
}
