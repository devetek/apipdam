<?php

namespace App\Http\Controllers\API\Book\admin;

use App\Models\BookCover;
use App\Repositories\Book\admin\BookCoverRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class BookCoverController
 * @package book/admin/ App\Http\Controllers\API\Book\admin
 */

class BookCoverAPIController extends AppBaseController
{
    /** @var  BookCoverRepository */
    private $bookCoverRepository;

    public function __construct(BookCoverRepository $bookCoverRepo)
    {
        $this->bookCoverRepository = $bookCoverRepo;
    }


    /**
     * @param CreateBookCoverAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/book/admin/bookCovers",
     *      summary="Store a newly created BookCover in storage",
     *      tags={"Book/admin/Book"},
     *      description="Store Book",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="BookCover that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Book")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Book"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(Request $request)
    {
            $input = $request->all();

            $bookCovers = $this->bookCoverRepository->create($request, $input);

            return $this->sendResponse($bookCovers, 'Book Cover saved successfully');
    }


}
