<?php

namespace App\Http\Controllers\API\Book\member;

use App\Models\BookContent;
use App\Repositories\Book\member\BookContentRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class BookContentController
 * @package book/member/ App\Http\Controllers\API\Book\member
 */

class BookContentAPIController extends AppBaseController
{
    /** @var  BookContentRepository */
    private $bookContentRepository;

    public function __construct(BookContentRepository $bookContentRepo)
    {
        $this->bookContentRepository = $bookContentRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/book/member/bookContents",
     *      summary="Get a listing of the BookContents.",
     *      tags={"Book/member/BookContent"},
     *      description="Get all BookContents",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/BookContent")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $bookContents = $this->bookContentRepository->all();
        return $this->sendResponse($bookContents, 'Book Contents retrieved successfully');
    }

    public function create(Request $reques)
    {

    }

    /**
     * @param CreateBookContentAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/book/member/bookContents",
     *      summary="Store a newly created BookContent in storage",
     *      tags={"Book/member/BookContent"},
     *      description="Store BookContent",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="BookContent that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/BookContent")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/BookContent"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(Request $request)
    {
            $input = $request->all();

            $bookContents = $this->bookContentRepository->create($input);

            return $this->sendResponse($bookContents, 'Book Content saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/book/member/bookContents/{id}",
     *      summary="Display the specified BookContent",
     *      tags={"Book/member/BookContent"},
     *      description="Get BookContent",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of BookContent",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/BookContent"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var BookContent $bookContent */
        $bookContent = $this->bookContentRepository->findWithoutFail($id);

        if (empty($bookContent)) {
            return $this->sendError('Book Content not found');
        }

        return $this->sendResponse($bookContent->toArray(), 'Book Content retrieved successfully');
    }


    /**
     * @param int $id
     * @param UpdateBookContentAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/book/member/bookContents/{id}",
     *      summary="Update the specified BookContent in storage",
     *      tags={"Book/member/BookContent"},
     *      description="Update BookContent",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of BookContent",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="BookContent that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/BookContent")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/BookContent"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, Request $request)
    {
        $input = $request->all();

        /** @var BookContent $bookContent */
        $bookContent = $this->bookContentRepository->findWithoutFail($id);

        if (empty($bookContent)) {
            return $this->sendError('Book Content not found');
        }

        $bookContent = $this->bookContentRepository->update($input, $id);

        return $this->sendResponse($bookContent->toArray(), 'BookContent updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/book/member/bookContents/{id}",
     *      summary="Remove the specified BookContent from storage",
     *      tags={"Book/member/BookContent"},
     *      description="Delete BookContent",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of BookContent",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var BookContent $bookContent */
        $bookContent = $this->bookContentRepository->findWithoutFail($id);

        if (empty($bookContent)) {
            return $this->sendError('Book Content not found');
        }

        $bookContent->delete();

        return $this->sendResponse($id, 'Book Content deleted successfully');
    }
}
