<?php

namespace App\Http\Controllers\API\Packet\member_Guest;

use App\Models\Packet_requirements;
use App\Repositories\Packet\member_Guest\Packet_requirementsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class Packet_requirementsController
 * @package packet/memberGuest/ App\Http\Controllers\API\Packet\member_Guest
 */

class Packet_requirementsAPIController extends AppBaseController
{
    /** @var  Packet_requirementsRepository */
    private $packetRequirementsRepository;

    public function __construct(Packet_requirementsRepository $packetRequirementsRepo)
    {
        $this->packetRequirementsRepository = $packetRequirementsRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/packet/memberGuest/packetRequirements",
     *      summary="Get a listing of the Packet_requirements.",
     *      tags={"Packet/member_Guest/Packet_requirements"},
     *      description="Get all Packet_requirements",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Packet_requirements")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $packetRequirements = $this->packetRequirementsRepository->all($request);
        return $this->sendResponse($packetRequirements, 'Packet Requirements retrieved successfully');
    }


}
