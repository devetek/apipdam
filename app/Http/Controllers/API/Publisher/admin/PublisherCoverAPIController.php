<?php

namespace App\Http\Controllers\API\Publisher\admin;

use App\Models\PublisherCover;
use App\Repositories\Publisher\admin\PublisherCoverRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class PublisherCoverController
 * @package publisher/admin/ App\Http\Controllers\API\Publisher\admin
 */

class PublisherCoverAPIController extends AppBaseController
{
    /** @var  PublisherCoverRepository */
    private $publisherCoverRepository;

    public function __construct(PublisherCoverRepository $publisherCoverRepo)
    {
        $this->publisherCoverRepository = $publisherCoverRepo;
    }


    /**
     * @param CreatePublisherCoverAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/publisher/admin/publisherCovers",
     *      summary="Store a newly created PublisherCover in storage",
     *      tags={"Publisher/admin/PublisherCover"},
     *      description="Store PublisherCover",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="PublisherCover that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/PublisherCover")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/PublisherCover"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(Request $request)
    {
            $input = $request->all();

            $publisherCovers = $this->publisherCoverRepository->create($request,$input);

            return $this->sendResponse($publisherCovers, 'Publisher Cover saved successfully');
    }

}
