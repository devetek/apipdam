<?php

namespace App\Http\Controllers\API\Publisher\admin;

use App\Models\Company;
use App\Models\Publisher;
use App\Models\v_publisher;
use App\Repositories\Company\admin\CompanyRepository;
use App\Repositories\Publisher\admin\PublisherRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class PublisherController
 * @package publisher/admin/ App\Http\Controllers\API\Publisher\admin
 */

class PublisherAPIController extends AppBaseController
{
    /** @var  PublisherRepository */
    private $publisherRepository;

    public function __construct(PublisherRepository $publisherRepo)
    {
        $this->publisherRepository = $publisherRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/publisher/admin/publishers",
     *      summary="Get a listing of the Publishers.",
     *      tags={"Publisher/admin/Publisher"},
     *      description="Get all Publishers",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/v_publisher")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $publishers = $this->publisherRepository->all();
        return $this->sendResponse($publishers, 'Publishers retrieved successfully');
    }

    public function create(Request $reques)
    {
        $data['company'] = Company::all();
        return $this->sendResponse($data, 'Publishers retrieved successfully');
    }

    //php artisan infyom:api Company --fromTable --tableName=company --prefix=company/admin --primary=id_company

    /**
     * @param CreatePublisherAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/publisher/admin/publishers",
     *      summary="Store a newly created Publisher in storage",
     *      tags={"Publisher/admin/Publisher"},
     *      description="Store Publisher",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Publisher that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Publisher")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Publisher"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(Request $request)
    {
            $input = $request->all();

            $publishers = $this->publisherRepository->create($input);

            return $this->sendResponse($publishers, 'Publisher saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/publisher/admin/publishers/{id}",
     *      summary="Display the specified Publisher",
     *      tags={"Publisher/admin/Publisher"},
     *      description="Get Publisher",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Publisher",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/v_publisher"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Publisher $publisher */
        $publisher = $this->publisherRepository->findWithoutFail($id);

        if (empty($publisher)) {
            return $this->sendError('Publisher not found');
        }

        return $this->sendResponse($publisher->toArray(), 'Publisher retrieved successfully');
    }


    /**
     * @param int $id
     * @param UpdatePublisherAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/publisher/admin/publishers/{id}",
     *      summary="Update the specified Publisher in storage",
     *      tags={"Publisher/admin/Publisher"},
     *      description="Update Publisher",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Publisher",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Publisher that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Publisher")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Publisher"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, Request $request)
    {
        $input = $request->all();

        /** @var Publisher $publisher */
        $publisher = $this->publisherRepository->findWithoutFail($id);

        if (empty($publisher)) {
            return $this->sendError('Publisher not found');
        }

        $publisher = $this->publisherRepository->update($input, $id);

        return $this->sendResponse($publisher->toArray(), 'Publisher updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/publisher/admin/publishers/{id}",
     *      summary="Remove the specified Publisher from storage",
     *      tags={"Publisher/admin/Publisher"},
     *      description="Delete Publisher",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Publisher",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Publisher $publisher */
        $publisher = $this->publisherRepository->findWithoutFail($id);

        if (empty($publisher)) {
            return $this->sendError('Publisher not found');
        }

        $publisher->delete();

        return $this->sendResponse($id, 'Publisher deleted successfully');
    }
}
