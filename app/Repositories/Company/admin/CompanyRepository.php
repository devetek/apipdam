<?php

namespace App\Repositories\Company\admin;

use App\Helper\Query;
use App\Models\Company;
use Illuminate\Support\Facades\DB;

/**
 * Class CompanyRepository
 * @package App\Repositories\Company\admin
 * @version February 17, 2019, 7:34 am UTC
 *
 * @method Company find($id, $columns = ['*'])
 * @method Company first($columns = ['*'])
*/
class CompanyRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name_company',
        'logo_company',
        'thum_company',
        'address_company',
        'about_company',
        'link_company',
        'id_head_company',
        'phone_company',
        'status_company',
        'id_user',
        'validasi'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Company::class;
    }

    public function all()
    {
        $sql = "Select * from company

                where validasi > 0
                order by created_at DESC
                ";
        $data = DB::select($sql);
        return $data;
    }

    public function findWithoutFail($id, $columns = ['*'])
    {
            try {
                return $this->model()::find($id);
            } catch (Exception $e) {
                return;
            }
    }

    public function create(array $attributes)
        {
            $model = new Company($attributes);
            $cek = Query::beforeInsert($model);
            return $cek;
        }

        public function update(array $attributes, $id)
        {
            $data = Company::find($id);
            $data->fill($attributes);
            if ($data->validasi == 0) {
                $data->validasi = 1;
            }
            $data->save();
            return $data;
        }

}
