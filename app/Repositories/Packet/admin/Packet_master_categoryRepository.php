<?php

namespace App\Repositories\Packet\admin;

use App\Helper\Query;
use App\Models\Packet_master_category;
use Illuminate\Support\Facades\DB;

/**
 * Class Packet_master_categoryRepository
 * @package App\Repositories\Packet\admin
 * @version February 19, 2019, 3:34 pm UTC
 *
 * @method Packet_master_category find($id, $columns = ['*'])
 * @method Packet_master_category first($columns = ['*'])
*/
class Packet_master_categoryRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name_category',
        'description_category'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Packet_master_category::class;
    }

    public function all()
    {
        $sql = "Select * from packet_master_category

                where validasi > 0
                order by created_at DESC
                ";
        $data = DB::select($sql);
        return $data;
    }

    public function findWithoutFail($id, $columns = ['*'])
    {
            try {
                return $this->model()::find($id);
            } catch (Exception $e) {
                return;
            }
    }

    public function create(array $attributes)
        {
            $model = new Packet_master_category($attributes);
            $cek = Query::beforeInsert($model);
            return $cek;
        }

        public function update(array $attributes, $id)
        {
            $data = Packet_master_category::find($id);
            $data->fill($attributes);
            if ($data->validasi == 0) {
                $data->validasi = 1;
            }
            $data->save();
            return $data;
        }

}
