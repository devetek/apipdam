<?php

namespace App\Repositories\Packet\member_Guest;

use App\Helper\Query;
use App\Models\Packet;
use App\Models\Packet_master_category;
use App\Models\V_packet_sale;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;

/**
 * Class PacketRepository
 * @package App\Repositories\Packet\member_Guest
 * @version February 19, 2019, 9:52 am UTC
 *
 * @method Packet find($id, $columns = ['*'])
 * @method Packet first($columns = ['*'])
 */
class PacketRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name_packet',
        'excerpt_packet',
        'description_packet',
        'id_publisher_team',
        'cover_packet',
        'thum_packet',
        'date_publish',
        'id_company',
        'status_packet',
        'is_public',
        'price',
        'validasi'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Packet::class;
    }

    public function all()
    {
        $sql = "Select * from v_packet_sale 
                where validasi > 0
                order by created_at DESC
                ";
        $data = DB::select($sql);
        $out = [];

        foreach ($data as $x) {
            if ($x->cover_packet == null) {
                $x->cover_packet = URL::to('/') . "/app/packet/default.png";
            } else {
                $x->cover_packet = URL::to('/') . "/app/packet/" . $x->id_packet . "/" . $x->cover_packet;

            }
            if ($x->thum_packet == null) {
                $x->thum_packet = URL::to('/') . "/app/packet/thum_default.png";
            } else {
                $x->thum_packet = URL::to('/') . "/app/packet/" . $x->id_packet . "/" . $x->thum_packet;

            }
            $out[] = $x;
        }

//        $data = Packet_master_category::where('id_packet_master_category', 1)->get(); //hanya kategori hompes
//        $data[0]->packet = $out;
        return $out;
    }

    public function findById($id)
    {
        $packet = V_packet_sale::where('id_packet',$id)->get();

        return $packet[0];

    }

    public function create(array $attributes)
    {
        $model = new Packet($attributes);
        $cek = Query::beforeInsert($model);
        return $cek;
    }

    public function update(array $attributes, $id)
    {
        $data = Packet::find($id);
        $data->fill($attributes);
        if ($data->validasi == 0) {
            $data->validasi = 1;
        }
        $data->save();
        return $data;
    }

}
