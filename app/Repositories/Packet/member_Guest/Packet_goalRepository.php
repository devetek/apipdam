<?php

namespace App\Repositories\Packet\member_Guest;

use App\Helper\Query;
use App\Models\Packet_goal;
use Illuminate\Support\Facades\DB;

/**
 * Class Packet_goalRepository
 * @package App\Repositories\Packet\member_Guest
 * @version February 21, 2019, 7:01 am UTC
 *
 * @method Packet_goal find($id, $columns = ['*'])
 * @method Packet_goal first($columns = ['*'])
*/
class Packet_goalRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_packet',
        'order_goal',
        'goal'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Packet_goal::class;
    }

    public function all($request)
    {
        $sql = "Select * from packet_goal

                where id_packet = ".$request->input('id_packet')."
                order by order_goal ASC
                ";
        $data = DB::select($sql);
        return $data;
    }

    public function findWithoutFail($id, $columns = ['*'])
    {
            try {
                return $this->model()::find($id);
            } catch (Exception $e) {
                return;
            }
    }

    public function create(array $attributes)
        {
            $model = new Packet_goal($attributes);
            $cek = Query::beforeInsert($model);
            return $cek;
        }

        public function update(array $attributes, $id)
        {
            $data = Packet_goal::find($id);
            $data->fill($attributes);
            if ($data->validasi == 0) {
                $data->validasi = 1;
            }
            $data->save();
            return $data;
        }

}
