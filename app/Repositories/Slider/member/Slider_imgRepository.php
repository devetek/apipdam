<?php

namespace App\Repositories\Slider\member;

use App\Helper\Query;
use App\Models\Slider_img;
use Illuminate\Support\Facades\DB;

/**
 * Class Slider_imgRepository
 * @package App\Repositories\Slider\member
 * @version February 19, 2019, 8:59 am UTC
 *
 * @method Slider_img find($id, $columns = ['*'])
 * @method Slider_img first($columns = ['*'])
*/
class Slider_imgRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_gallery',
        'img',
        'thum',
        'status'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Slider_img::class;
    }

    public function all()
    {
        $sql = "Select * from gallery_img

                where validasi > 0
                order by created_at DESC
                ";
        $data = DB::select($sql);
        return $data;
    }

    public function findWithoutFail($id, $columns = ['*'])
    {
            try {
                return $this->model()::find($id);
            } catch (Exception $e) {
                return;
            }
    }

    public function create(array $attributes)
        {
            $model = new Slider_img($attributes);
            $cek = Query::beforeInsert($model);
            return $cek;
        }

        public function update(array $attributes, $id)
        {
            $data = Slider_img::find($id);
            $data->fill($attributes);
            if ($data->validasi == 0) {
                $data->validasi = 1;
            }
            $data->save();
            return $data;
        }

}
