<?php

namespace App\Repositories\Book\admin;

use App\Helper\Query;
use App\Models\BookContent;
use App\Models\BookSection;
use Illuminate\Support\Facades\DB;

/**
 * Class BookSectionRepository
 * @package App\Repositories\Book\admin
 * @version February 9, 2019, 2:33 pm UTC
 *
 * @method BookSection find($id, $columns = ['*'])
 * @method BookSection first($columns = ['*'])
*/
class BookSectionRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_book',
        'order_book_section',
        'book_section'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return BookSection::class;
    }

    public function all($request)
    {
        $sql = "Select * from book_section

                where validasi > 0
                and id_book=".$request->input('id_book')."
                order by order_book_section ASC
                ";
        $data = DB::select($sql);

        $out = [];
        foreach ($data as $x)
        {
            $sql="select
                      *
                    from book_content
                    where id_book_section=".$x->id_book_section."
                    and validasi > 0
                    order by order_book_content ASC";

            $content = DB::select($sql);
            $x->content = $content;
            $out[] = $x;
        }

        return $out;
    }

    public function findWithoutFail($id, $columns = ['*'])
    {
            try {
                return $this->model()::find($id);
            } catch (Exception $e) {
                return;
            }
    }

    public function create(array $attributes)
        {
            $model = new BookSection($attributes);
            $cek = Query::beforeInsert($model);
            return $cek;
        }

        public function update(array $attributes, $id)
        {
            $data = BookSection::find($id);
            $data->fill($attributes);
            if ($data->validasi == 0) {
                $data->validasi = 1;
            }
            $data->save();
            return $data;
        }

}
