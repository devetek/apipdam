<?php

namespace App\Repositories\Book\member;

use App\Helper\Query;
use App\Models\BookSection;
use Illuminate\Support\Facades\DB;

/**
 * Class BookSectionRepository
 * @package App\Repositories\Book\member
 * @version February 9, 2019, 2:46 pm UTC
 *
 * @method BookSection find($id, $columns = ['*'])
 * @method BookSection first($columns = ['*'])
*/
class BookSectionRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_book',
        'order_book_section',
        'book_section',
        'validasi'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return BookSection::class;
    }

    public function all()
    {
        $sql = "Select * from book_section

                where validasi > 0
                order by created_at DESC
                ";
        $data = DB::select($sql);
        return $data;
    }

    public function findWithoutFail($id, $columns = ['*'])
    {
            try {
                return $this->model()::find($id);
            } catch (Exception $e) {
                return;
            }
    }

    public function create(array $attributes)
        {
            $model = new BookSection($attributes);
            $cek = Query::beforeInsert($model);
            return $cek;
        }

        public function update(array $attributes, $id)
        {
            $data = BookSection::find($id);
            $data->fill($attributes);
            if ($data->validasi == 0) {
                $data->validasi = 1;
            }
            $data->save();
            return $data;
        }

}
