<?php

namespace App\Models;

use Eloquent as Model;

/**
 * @SWG\Definition(
 *      definition="Slider",
 *      required={""},
 *      @SWG\Property(
 *          property="id_gallery",
 *          description="id_gallery",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="for_module",
 *          description="for_module",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="for_id_module",
 *          description="for_id_module",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="name_galley",
 *          description="name_galley",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="code",
 *          description="code",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="id_user",
 *          description="id_user",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */
class Slider extends Model
{

    public $table = 'gallery';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $primaryKey = 'id_gallery';

    public $fillable = [
        'for_module',
        'for_id_module',
        'name_galley',
        'code',
        'id_user'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id_gallery' => 'integer',
        'for_module' => 'string',
        'for_id_module' => 'integer',
        'name_galley' => 'string',
        'code' => 'string',
        'id_user' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
