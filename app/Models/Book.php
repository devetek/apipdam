<?php

namespace App\Models;

use Eloquent as Model;

/**
 * @SWG\Definition(
 *      definition="Book",
 *      required={""},
 *      @SWG\Property(
 *          property="id_book",
 *          description="id_book",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="title_book",
 *          description="title_book",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="org_price",
 *          description="org_price",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="actual_price",
 *          description="actual_price",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="description",
 *          description="description",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="promotional_text",
 *          description="promotional_text",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="id_publisher",
 *          description="id_publisher",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="id_user",
 *          description="id_user",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="cover_url",
 *          description="cover_url",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="thum_cover_url",
 *          description="thum_cover_url",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="date_publish",
 *          description="date_publish",
 *          type="string",
 *          format="date"
 *      ),
 *      @SWG\Property(
 *          property="status",
 *          description="status",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="validasi",
 *          description="validasi",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */
class Book extends Model
{

    public $table = 'book';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $primaryKey = 'id_book';

    public $fillable = [
        'title_book',
        'org_price',
        'actual_price',
        'description',
        'promotional_text',
        'id_publisher',
        'id_user',
        'cover_url',
        'thum_cover_url',
        'date_publish',
        'status',
        'validasi'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id_book' => 'integer',
        'title_book' => 'string',
        'org_price' => 'integer',
        'actual_price' => 'integer',
        'description' => 'string',
        'promotional_text' => 'string',
        'id_publisher' => 'integer',
        'id_user' => 'integer',
        'cover_url' => 'string',
        'thum_cover_url' => 'string',
        'date_publish' => 'date',
        'status' => 'string',
        'validasi' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
