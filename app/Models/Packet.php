<?php

namespace App\Models;

use Eloquent as Model;

/**
 * @SWG\Definition(
 *      definition="Packet",
 *      required={""},
 *      @SWG\Property(
 *          property="id_packet",
 *          description="id_packet",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="name_packet",
 *          description="name_packet",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="excerpt_packet",
 *          description="excerpt_packet",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="description_packet",
 *          description="description_packet",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="id_publisher_team",
 *          description="id_publisher_team",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="cover_packet",
 *          description="cover_packet",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="thum_packet",
 *          description="thum_packet",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="date_publish",
 *          description="date_publish",
 *          type="string",
 *          format="date"
 *      ),
 *      @SWG\Property(
 *          property="id_company",
 *          description="id_company",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="status_packet",
 *          description="status_packet",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="is_public",
 *          description="is_public",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="price",
 *          description="price",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="validasi",
 *          description="validasi",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */
class Packet extends Model
{

    public $table = 'packet';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $primaryKey = 'id_packet';

    public $fillable = [
        'name_packet',
        'excerpt_packet',
        'description_packet',
        'id_publisher_team',
        'cover_packet',
        'thum_packet',
        'date_publish',
        'id_company',
        'status_packet',
        'is_public',
        'price',
        'validasi'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id_packet' => 'integer',
        'name_packet' => 'string',
        'excerpt_packet' => 'string',
        'description_packet' => 'string',
        'id_publisher_team' => 'integer',
        'cover_packet' => 'string',
        'thum_packet' => 'string',
        'date_publish' => 'date',
        'id_company' => 'integer',
        'status_packet' => 'string',
        'is_public' => 'string',
        'price' => 'integer',
        'validasi' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
