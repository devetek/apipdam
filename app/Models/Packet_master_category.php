<?php

namespace App\Models;

use Eloquent as Model;

/**
 * @SWG\Definition(
 *      definition="Packet_master_category",
 *      required={""},
 *      @SWG\Property(
 *          property="id_packet_master_category",
 *          description="id_packet_master_category",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="name_category",
 *          description="name_category",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="description_category",
 *          description="description_category",
 *          type="string"
 *      )
 * )
 */
class Packet_master_category extends Model
{

    public $table = 'packet_master_category';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $primaryKey = 'id_packet_master_category';

    public $fillable = [
        'name_category',
        'description_category'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id_packet_master_category' => 'integer',
        'name_category' => 'string',
        'description_category' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
