<?php

namespace App\Models;

use Eloquent as Model;

/**
 * @SWG\Definition(
 *      definition="Publisher",
 *      required={""},
 *      @SWG\Property(
 *          property="id_publisher",
 *          description="id_publisher",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="first_name",
 *          description="first_name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="last_name",
 *          description="last_name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="public_name",
 *          description="public_name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="title_publisher",
 *          description="title_publisher",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="foto_publisher",
 *          description="foto_publisher",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="thum_publisher",
 *          description="thum_publisher",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="phone_number",
 *          description="phone_number",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="mail_publisher",
 *          description="mail_publisher",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="id_company",
 *          description="id_company",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="join_date",
 *          description="join_date",
 *          type="string",
 *          format="date"
 *      ),
 *      @SWG\Property(
 *          property="status_publisher",
 *          description="status_publisher",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="validasi",
 *          description="validasi",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */
class Publisher extends Model
{

    public $table = 'publisher';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $primaryKey = 'id_publisher';

    public $fillable = [
        'first_name',
        'last_name',
        'public_name',
        'title_publisher',
        'foto_publisher',
        'thum_publisher',
        'phone_number',
        'mail_publisher',
        'id_company',
        'join_date',
        'status_publisher',
        'validasi'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id_publisher' => 'integer',
        'first_name' => 'string',
        'last_name' => 'string',
        'public_name' => 'string',
        'title_publisher' => 'string',
        'foto_publisher' => 'string',
        'thum_publisher' => 'string',
        'phone_number' => 'string',
        'mail_publisher' => 'string',
        'id_company' => 'integer',
        'join_date' => 'date',
        'status_publisher' => 'string',
        'validasi' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
