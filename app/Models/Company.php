<?php

namespace App\Models;

use Eloquent as Model;

/**
 * @SWG\Definition(
 *      definition="Company",
 *      required={""},
 *      @SWG\Property(
 *          property="id_company",
 *          description="id_company",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="name_company",
 *          description="name_company",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="logo_company",
 *          description="logo_company",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="thum_company",
 *          description="thum_company",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="address_company",
 *          description="address_company",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="about_company",
 *          description="about_company",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="link_company",
 *          description="link_company",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="id_head_company",
 *          description="id_head_company",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="phone_company",
 *          description="phone_company",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="status_company",
 *          description="status_company",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="id_user",
 *          description="id_user",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="validasi",
 *          description="validasi",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */
class Company extends Model
{

    public $table = 'company';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $primaryKey = 'id_company';

    public $fillable = [
        'name_company',
        'logo_company',
        'thum_company',
        'address_company',
        'about_company',
        'link_company',
        'id_head_company',
        'phone_company',
        'status_company',
        'id_user',
        'validasi'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id_company' => 'integer',
        'name_company' => 'string',
        'logo_company' => 'string',
        'thum_company' => 'string',
        'address_company' => 'string',
        'about_company' => 'string',
        'link_company' => 'string',
        'id_head_company' => 'string',
        'phone_company' => 'string',
        'status_company' => 'string',
        'id_user' => 'integer',
        'validasi' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
