<?php

namespace App\Models;

use Eloquent as Model;

/**
 * @SWG\Definition(
 *      definition="V_book",
 *      required={""},
 *      @SWG\Property(
 *          property="id_book",
 *          description="id_book",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="title_book",
 *          description="title_book",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="org_price",
 *          description="org_price",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="actual_price",
 *          description="actual_price",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="description",
 *          description="description",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="promotional_text",
 *          description="promotional_text",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="id_publisher",
 *          description="id_publisher",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="id_user",
 *          description="id_user",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="cover_url",
 *          description="cover_url",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="thum_cover_url",
 *          description="thum_cover_url",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="date_publish",
 *          description="date_publish",
 *          type="string",
 *          format="date"
 *      ),
 *      @SWG\Property(
 *          property="status",
 *          description="status",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="validasi",
 *          description="validasi",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="first_name",
 *          description="first_name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="foto_publisher",
 *          description="foto_publisher",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="last_name",
 *          description="last_name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="phone_number",
 *          description="phone_number",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="public_name",
 *          description="public_name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="title_publisher",
 *          description="title_publisher",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="user_input",
 *          description="user_input",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="about_company",
 *          description="about_company",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="name_company",
 *          description="name_company",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="logo_company",
 *          description="logo_company",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="address_company",
 *          description="address_company",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="phone_company",
 *          description="phone_company",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="id_company",
 *          description="id_company",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */
class V_book extends Model
{

    public $table = 'v_book';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $primaryKey = 'id_book';

    public $fillable = [
        'id_book',
        'title_book',
        'org_price',
        'actual_price',
        'description',
        'promotional_text',
        'id_publisher',
        'id_user',
        'cover_url',
        'thum_cover_url',
        'date_publish',
        'status',
        'validasi',
        'first_name',
        'foto_publisher',
        'last_name',
        'phone_number',
        'public_name',
        'title_publisher',
        'user_input',
        'about_company',
        'name_company',
        'logo_company',
        'address_company',
        'phone_company',
        'id_company'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id_book' => 'integer',
        'title_book' => 'string',
        'org_price' => 'integer',
        'actual_price' => 'integer',
        'description' => 'string',
        'promotional_text' => 'string',
        'id_publisher' => 'integer',
        'id_user' => 'integer',
        'cover_url' => 'string',
        'thum_cover_url' => 'string',
        'date_publish' => 'date',
        'status' => 'string',
        'validasi' => 'integer',
        'first_name' => 'string',
        'foto_publisher' => 'string',
        'last_name' => 'string',
        'phone_number' => 'string',
        'public_name' => 'string',
        'title_publisher' => 'string',
        'user_input' => 'string',
        'about_company' => 'string',
        'name_company' => 'string',
        'logo_company' => 'string',
        'address_company' => 'string',
        'phone_company' => 'string',
        'id_company' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
